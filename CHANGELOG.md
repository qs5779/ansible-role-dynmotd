<!-- markdownlint-configure-file { "MD024": false } -->
# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.3] - 2024-09-19

- Only run from profile.d if bash or zsh

## [1.0.2] - 2024-08-28

### Fixed

- issues with raspberry 30-sysinfo

### Removed

- PYTHON_PATH from evn of cron job

## [1.0.1] - 2024-08-27

### Changed

- Use new pipx global apps

## [1.0.0] - 2024-08-19

### Added

- Initial version tracking and CHANGELOG

### Changed

- removed bashisms from profile.d-motd.sh
