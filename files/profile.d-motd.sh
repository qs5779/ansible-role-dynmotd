#!/bin/sh
# vim:sta:et:sw=2:ts=2:ft=sh
#
# Revision History:
# 20201230 - que - shellcheck corrections
# 20220823 - que - check variable for run needed
# 20240819 - que - remove bashisms

[ -n "${BASH_VERSION:-}" ] || [ -n "${ZSH_VERSION:-}" ] || return 0
[ -t 1 ] && [ -d /etc/update-motd.d ] && [ -z "$WTF_DYNMOTD" ] && run-parts /etc/update-motd.d
