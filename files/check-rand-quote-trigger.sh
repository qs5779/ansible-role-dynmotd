#!/bin/sh
# shellcheck shell=dash
# shellcheck source=/dev/null

[ -f "${HOME}/quotes.off" ] && exit 0


SCRIPT=$(basename "$0" .sh)
FORCE=0
ODBG="--no-debug"

usage() {
  cat << EOM
usage: $SCRIPT [-f]
  where:
    -f force the run
    -h show this message and exit
EOM
  exit 1
}

while getopts ":fh" opt
do
  case "$opt" in
    f )
      FORCE=$((FORCE+=1))
    ;;
    h )
      usage
    ;;
    * )
      echo "Unexpected option \"$opt\""
      usage
    ;;
  esac
done
shift $((OPTIND - 1))

if ! groups | grep -q users
then
  sudo usermod -a -G users "$(whoami)"
fi

TRIGGER=/var/cache/users/randquote.trigger
RUN="$FORCE"
REFRESH="0"

if [ "$RUN" = "0" ]
then
  [ -s "$TRIGGER" ] && RUN=1
  if [ "$RUN" = "0" ]
  then
    [ -s "/var/cache/wtfo/cached-quotes.refresh" ] && REFRESH=1
  fi
fi

if [ "$RUN" != "0" ] || [ "$REFRESH" != "0" ]
then
  if command -v wtfquote >/dev/null 2>&1
  then
    if [ -f "${HOME}/quotes.debug" ] ; then
      ODBG="--debug"
      ls -l /var/cache/wtfo/ > /tmp/wtfo.cache.listing
    fi
    for ext in json refresh
    do
      TGT="/var/cache/wtfo/cached-quotes.${ext}"
      if [ -f "$TGT" ]; then
        if [ ! -w "$TGT" ] ; then
          sudo chmod 660 "$TGT"
          sudo chown root:sudo "$TGT"
        fi
      fi
    done

    if [ "$RUN" != "0" ]
    then
      wtfquote $ODBG printone -f /etc/randquote
    else
      wtfquote $ODBG cache --refresh
    fi
  else
    echo "When it stops hurting, it will feel better. - Quien Sabe" > /etc/randquote
  fi
  cat /dev/null > "$TRIGGER"
  if [ ! -w "$TRIGGER" ]; then
    sudo chmod 660 "$TRIGGER"
    sudo chown root:users "$TRIGGER"
  fi
fi
