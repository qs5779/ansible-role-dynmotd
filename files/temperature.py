#!/usr/bin/env python

import os


def get_thermal_zones(thermal_zone_path=None):
    if thermal_zone_path is None:
        if os.path.isdir("/sys/class/thermal"):
            thermal_zone_path = "/sys/class/thermal"
        else:
            thermal_zone_path = "/proc/acpi/thermal_zone"
    if os.path.isdir(thermal_zone_path):
        for zone_name in sorted(os.listdir(thermal_zone_path)):
            yield ThermalZone(thermal_zone_path, zone_name)


class ThermalZone:
    temperature = None
    temperature_value = None
    temperature_unit = None

    def __init__(self, base_path, name):
        self.name = name
        self.path = os.path.join(base_path, name)
        temperature_path = os.path.join(self.path, "temp")
        if os.path.isfile(temperature_path):
            try:
                with open(temperature_path) as f:
                    line = f.readline()
                    try:
                        self.temperature_value = int(line.strip()) / 1000.0
                        self.temperature_unit = "C"
                        self.temperature = "{:.1f} {}".format(
                            self.temperature_value,
                            self.temperature_unit,
                        )
                    except ValueError:
                        pass
            except OSError:
                pass
        else:
            temperature_path = os.path.join(self.path, "temperature")
            if os.path.isfile(temperature_path):
                for line in open(temperature_path):
                    if line.startswith("temperature:"):
                        self.temperature = line[12:].strip()
                        try:
                            value, unit = self.temperature.split()
                            self.temperature_value = int(value)
                            self.temperature_unit = unit
                        except ValueError:
                            pass


class Temperature:
    def __init__(self, thermal_zone_path=None):
        self._thermal_zone_path = thermal_zone_path

    # def register(self, sysinfo):
    #     self._sysinfo = sysinfo

    def run(self):
        temperature = None
        max_value = None
        for zone in get_thermal_zones(self._thermal_zone_path):
            if zone.temperature_value is not None and (
                max_value is None or zone.temperature_value > max_value,
            ):
                temperature = zone.temperature
                max_value = zone.temperature_value
        if temperature is not None:
            # self._sysinfo.add_header("Temperature", temperature)
            print("Temperature: {0}".format(temperature))
        # return succeed(None)


temp = Temperature()
temp.run()
